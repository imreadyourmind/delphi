unit Http;
//v 1.0.9 //��������� ���������: Header.Clear �������
interface

uses
windows, httpsend, ssl_openssl, SysUtils, Classes;

type
TDHttp = class
  private
    FCodePage: Cardinal;
    FSend: THttpSend;
  public
    CSRFToken:string;
    MimeType:string;
    UserAgent:string;
    Referer,Cookie,RedirectUrl,EmulationType:string;
    APath:string;
    AFiles:TStringList;
    TimeOut:integer;
    RedirectDepth:integer;
    ProxyHost,ProxyPort,SocksIP,SocksPort,ProxyUser,ProxyPass:string;
    HandleRedirect:boolean;
    Header,ResponseHeader : TStringList;
    constructor Create;
    destructor Destroy; override;
    function GetError: string;
    function GetRedirectUrl: string;
    procedure Emulation(Response:string);
    function Send(AUrl: string; AParams: string = ''): string;

end;

implementation
uses Common;

constructor TDHttp.Create;
begin
  MimeType := 'application/x-www-form-urlencoded';
  UserAgent := 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208';
  TimeOut := 30000;
  RedirectDepth := 3;
  HandleRedirect := false;

  FSend := THttpSend.create;
  FSend.AddPortNumberToHost := false;
  FSend.Protocol := '1.1';
  FSend.Timeout := TimeOut;
  FCodePage := CP_UTF8;
  FSend.UserAgent := UserAgent;
  FSend.MimeType := MimeType;

  Header := TStringList.Create;
  ResponseHeader := TStringList.Create;
  AFiles := TStringList.Create;   // AFiles.Add(fieldname=path_to_file;mime_type);
end;

destructor TDHttp.Destroy;
begin
  FSend.Free;
  inherited;
end;

procedure TDHttp.Emulation(Response:string);
begin
  if EmulationType = 'instagram' then begin
    UserAgent := 'Instagram 6.15.0 Android (19/4.4.2; 160dpi; 1152x672; samsung; GT-N7100; t03g; unknown; ru_RU)';
    CSRFToken := PregMatch('csrftoken=([\w]+);',FSend.Headers.Text,1);
  end else if EmulationType = 'odnoklassniki' then begin
    CSRFToken := PregMatch('tkn\.set\(''([\w\-]+)''\)',Response,1);
    if CSRFToken = '' then
      CSRFToken := PregMatch('TKN: ([\w\-]+)',FSend.Headers.Text,1);
  end;
end;

function TDHttp.GetError: string;
begin
  Result := '';
  if FSend.ResultCode>0 then
    Result := Result+IntToStr(FSend.ResultCode);
  if FSend.ResultString<>'' then
    Result := Result+': '+FSend.ResultString;
end;

function TDHttp.GetRedirectUrl: string;
var i: integer;
    Line: string;
begin
  Result := '';
  for i := 0 to FSend.Headers.Count-1 do
  begin
    Line := LowerCase(FSend.Headers[i]);
    if Pos('location:',Line)>0 then
    begin
      Result := Trim(StringReplace(FSend.Headers[i],'location:','',[rfIgnoreCase]));
      Exit;
    end;
  end;
end;

function TDHttp.Send(AUrl: string; AParams: string): string;
var Stream: TStringStream;
    FileStream: TFileStream;
    Method,Bound,Temp: AnsiString;
    I: integer;
    Content,Params : TStringList;
const
    FIELD_MASK = #13#10 + '--%s' + #13#10 + 'Content-Disposition: form-data; name="%s"' + #13#10 + #13#10 + '%s';
begin
  //�������� ��������
  Stream := TStringStream.Create(AParams,FCodePage);
  Content := TStringList.Create;
  Params := TStringList.Create;

  ResponseHeader.Clear;
  FSend.Headers.Clear;
  FSend.Document.Clear;
  FSend.Cookies.Text := Cookie;
  Method := 'GET';
  RedirectUrl := '';
  

  //������� ����������� ��������
  if Referer <> '' then begin FSend.Headers.Add('Referer: '+Referer); Referer := ''; end;
  if Timeout <> 0 then  FSend.Timeout:= TimeOut;
  if UserAgent <> '' then FSend.UserAgent:=UserAgent;
  if MimeType <> '' then FSend.MimeType:=MimeType;

  FSend.ProxyHost := ProxyHost;
  FSend.ProxyPort := ProxyPort;
  FSend.ProxyUser := ProxyUser;
  FSend.ProxyPass := ProxyPass;
  FSend.Sock.SocksIP := SocksIP;
  FSend.Sock.SocksPort := SocksPort;
  FSend.Sock.SocksUsername := ProxyUser;
  FSend.Sock.SocksPassword := ProxyPass;


  //CSRFToken ��� ��������� ��������
  if EmulationType = 'instagram' then begin
    Header.Add('X-Instagram-AJAX: 1') ;
    Header.Add('X-CSRFToken: '+CSRFToken) ;
  end else if EmulationType = 'odnoklassniki' then begin
    Header.Add('TKN: '+CSRFToken) ;
  end;

  FSend.Headers.AddStrings(Header);

  //�������� ������
  if AFiles.Count > 0 then begin
    Bound := IntToHex(Random(100000000), 8) + '_boundary';
    Temp := '--' + Bound + #13#10;
    I:=0;
    while I < AFiles.Count do begin
      Temp := Temp + 'Content-Disposition: form-data; name="' + PregMatch('^(.*?)\=',AFiles[i],1) +'";'+ #13#10;
      Temp := Temp + 'Filename="' +   ExtractFileName(PregMatch('\=(.*?);',AFiles[I],1)) +'"' + #13#10;
      Temp := Temp + 'Content-Type: '+ PregMatch('\;(.*?)$',AFiles[I],1) + #13#10 + #13#10;
      FSend.Document.Write(PAnsiChar(Temp)^, Length(Temp));
      if PregMatch('\=(.*?);',AFiles[I],1)<>'' then begin
        FileStream := TFileStream.Create(PregMatch('\=(.*?);',AFiles[I],1), fmOpenRead);
        FileStream.Position := 0;
        Fsend.Document.CopyFrom(FileStream, FileStream.Size);
        FileStream.Free;
      end;
      Inc(I);
    end;
    I:=0;
    if AParams <> '' then begin
      if Pos('&',AParams)<>0 then begin
        Params := Explode('&',AParams);
      end else
        Params.add(AParams);
      while I < Params.Count do begin
        Temp := Format(FIELD_MASK,[Bound, PregMatch('^(.*?)\=',Params[I],1),PregMatch('\=(.*?)$',Params[I],1)]);
        FSend.Document.Write(PAnsiChar(Temp)^, Length(Temp));
        Inc(I);
      end;
    end;
    Temp := #13#10 + '--' + Bound + '--' + #13#10;
    FSend.Document.Write(PAnsiChar(Temp)^, Length(Temp));
    FSend.MimeType := 'multipart/form-data, boundary=' + Bound;
  end else begin
    FSend.Document.LoadFromStream(Stream);
  end;

  if AParams <> '' then
    Method:='POST';

  if FSend.HTTPMethod(Method,AUrl) then
  begin
    Referer := AUrl;
    Stream.Clear;
    Header.Clear;
    Content.LoadFromStream(FSend.Document);
    ResponseHeader := FSend.Headers;
    Emulation(Content.text);
    Result := Content.text;
    RedirectUrl := GetRedirectUrl;
    Cookie := FSend.Cookies.Text;
    i := 0;
    if APath <> '' then
      FSend.Document.SaveToFile(APath);
    APath := '';
    while (RedirectUrl<>'') AND (HandleRedirect)do begin
      Referer := AUrl;
      FSend.HTTPMethod('GET',RedirectUrl);
      Content.LoadFromStream(FSend.Document);
      ResponseHeader := FSend.Headers;
      Header.Clear;
      Emulation(Content.text);
      Result := Content.text;
      Cookie := FSend.Cookies.Text;
      RedirectUrl := GetRedirectUrl;
      inc(i);
      if i = RedirectDepth then begin
        if APath <> '' then
          FSend.Document.SaveToFile(APath);
        break;
      end
    end;
  end;
  Stream.Free;
  Content.Free;
end;

end.
