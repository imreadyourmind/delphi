unit Common;

// v1.0.6

interface

uses
IDGlobal,RegExpr,httpsend,Http,System.Classes,System.StrUtils,System.SysUtils,synacode,IdHashMessageDigest;

function Explode(const delim, str: string): TStringList;
function PregMatch(expression:string;text:string;itemIndex:integer):string;
function PregMatchAll(expression:string;text:string;itemIndex:integer):TStringList;
function CustomMd5(s: string): string;

implementation

function CustomMd5(s: string): string;
begin
  Result := '';
  with TIdHashMessageDigest5.Create do
    try
      Result := LowerCase(HashStringAsHex(s,IndyTextEncoding_UTF8 ));
    finally
    Free;
  end;
end;

function Explode(const delim, str: string): TStringList;
var offset: integer;
    cur: integer;
    dl: integer;
begin
  Result:=TStringList.Create;
  dl:=Length(delim);
  offset:=1;
  while True do begin
      cur:=PosEx(delim, str, offset);
      if cur > 0 then
          Result.Add(Copy(str, offset, cur - offset))
      else begin
          Result.Add(Copy(str, offset, Length(str) - offset + 1));
          Break
      end;
      offset:=cur + dl;
  end;
end;

function PregMatch(Expression:string;Text:string;ItemIndex:Integer):string;
var
  RegExp: TRegExpr;
begin
  Result := '';
  RegExp := TRegExpr.Create;
  RegExp.Expression := expression;
  RegExp.ModifierS := true;
  if RegExp.Exec(Text) then
  begin
    Result := RegExp.Match[ItemIndex];
  end;
  RegExp.Free;
end;

function PregMatchAll(expression:string;text:string;itemIndex:integer):TStringList;
var
  RegExp: TRegExpr;
begin
  Result:=TStringList.Create;
  RegExp := TRegExpr.Create;
  RegExp.Expression := expression;
  RegExp.ModifierS := true;
  if RegExp.Exec(text) then
  begin
    repeat
      result.Add(RegExp.Match[itemIndex]);
    until not RegExp.ExecNext;
  end;
  RegExp.Free;
end;



end.
